<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\siswa;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
        return view('w');
});


Route::post('/user', [siswa::class, 'index']);
Route::get('/bukaa', [siswa::class, 'buka']);

Route::get('/up/{data}', [siswa::class, 'det']);
Route::put('/pput/{dat}', [siswa::class, 'up']);
Route::DELETE('/delet/{dataa}', [siswa::class, 'delete']);
